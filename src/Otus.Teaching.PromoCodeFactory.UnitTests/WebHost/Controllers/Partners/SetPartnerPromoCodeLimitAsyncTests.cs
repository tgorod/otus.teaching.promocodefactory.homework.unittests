﻿using AutoFixture.AutoMoq;
using AutoFixture;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using Xunit;
using FluentAssertions;
using System.Collections.Generic;
using static System.Runtime.InteropServices.JavaScript.JSType;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Classes;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private SetPartnerPromoCodeLimitRequest _requestWith0Limit => new SetPartnerPromoCodeLimitRequest { EndDate = DateTime.Now.AddMinutes(10), Limit = -1 };
        private SetPartnerPromoCodeLimitRequest _request => new SetPartnerPromoCodeLimitRequest { EndDate = DateTime.Now.AddMinutes(10), Limit = 15 };
        private static PartnerPromoCodeLimit _limitEnded => new PartnerPromoCodeLimit { Id = Guid.NewGuid(), CreateDate = DateTime.Now, EndDate = DateTime.Now.AddMinutes(-10), Limit = 20 };
        private static PartnerPromoCodeLimit _limitCanceled => new PartnerPromoCodeLimit { Id = Guid.NewGuid(), CreateDate = DateTime.Now, EndDate = DateTime.Now.AddMinutes(10), CancelDate = DateTime.Now, Limit = 6 };
        private static PartnerPromoCodeLimit _limitNotEnded => new PartnerPromoCodeLimit { Id = Guid.NewGuid(), CreateDate = DateTime.Now, EndDate = DateTime.Now.AddMinutes(10), Limit = 30 };
        public static IEnumerable<object[]> Data =>
            new List<object[]>
            {
                new object[] { _limitEnded, 5, 5 },
                new object[] { _limitCanceled, 8, 8 },
                new object[] { _limitNotEnded, 10, 0 },
            }; 

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        // 1 Если партнер не найден, то также нужно выдать ошибку 404
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, null);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        // 2 Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = PartnerBuilder
                            .Build()
                            .SetNotActive();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, null);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        // 3 Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes,
        // если лимит закончился, или отменен, то количество не обнуляется
        [Theory]
        [MemberData(nameof(Data))]
        public async void SetPartnerPromoCodeLimitAsync_SetLimit_ReturnsPromoCodes(PartnerPromoCodeLimit limit, int initial, int expected)
        {
            var callArgs = new List<Partner>();

            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = PartnerBuilder
                            .Build()
                            .SetNumberIssuedPromoCodes(initial)
                            .AddPartnerLimit(limit);

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);


            _partnersRepositoryMock.Setup(repo => repo.UpdateAsync(It.IsAny<Partner>()))
                .Callback<Partner>(callArgs.Add);


            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, _request);

            // Assert
            callArgs[0].NumberIssuedPromoCodes.Should().Be(expected);
        }

        // 4 При установке лимита нужно отключить предыдущий лимит
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimit_ReturnsCancelLimit()
        {
            var callArgs = new List<Partner>();

            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = PartnerBuilder
                            .Build()
                            .AddPartnerLimit(_limitNotEnded);

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);


            _partnersRepositoryMock.Setup(repo => repo.UpdateAsync(It.IsAny<Partner>()))
                .Callback<Partner>(callArgs.Add);


            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, _request);

            // Assert
            callArgs[0].PartnerLimits.FirstOrDefault(x => x.EndDate.CompareTo(DateTime.Now) > 0).CancelDate.HasValue.Should().BeTrue();
        }

        // 5 Лимит должен быть больше 0
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimitLess0_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = PartnerBuilder.Build();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, _requestWith0Limit);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        // 6 Нужно убедиться, что сохранили новый лимит в базу данных 
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimit_ReturnsSavedLimit()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = PartnerBuilder.Build();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, _request);
            var resultLimitId = (result as CreatedAtActionResult).RouteValues["limitId"];

            // Assert
            var newPartner = await _partnersRepositoryMock.Object.GetByIdAsync(partnerId);
            newPartner.PartnerLimits.FirstOrDefault(x => x.Id.Equals(resultLimitId)).Should().NotBeNull();
        }
    }
}